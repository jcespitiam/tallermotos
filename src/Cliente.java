public class Cliente {
    private String nombre, cedula, direccion,correo,numeroCelular,numeroFijo;

    public Cliente(String nombre, String cedula, String direccion, String correo, String numeroCelular, String numeroFijo) {
        this.nombre = nombre;
        this.cedula = cedula;
        this.direccion = direccion;
        this.correo = correo;
        this.numeroCelular = numeroCelular;
        this.numeroFijo = numeroFijo;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public void setNumeroCelular(String numeroCelular) {
        this.numeroCelular = numeroCelular;
    }

    public void setNumeroFijo(String numeroFijo) {
        this.numeroFijo = numeroFijo;
    }

    public String getNombre() {
        return nombre;
    }

    public String getCedula() {
        return cedula;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getCorreo() {
        return correo;
    }

    public String getNumeroCelular() {
        return numeroCelular;
    }

    public String getNumeroFijo() {
        return numeroFijo;
    }
}