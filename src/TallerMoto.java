public class TallerMoto {
    private String nombre,direccion,telefonoCelular,telefonoFijo,nit,email;

    public TallerMoto(String nombre, String direccion, String telefonoCelular, String telefonoFijo, String nit, String email) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefonoCelular = telefonoCelular;
        this.telefonoFijo = telefonoFijo;
        this.nit = nit;
        this.email = email;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public void setTelefonoCelular(String telefonoCelular) {
        this.telefonoCelular = telefonoCelular;
    }

    public void setTelefonoFijo(String telefonoFijo) {
        this.telefonoFijo = telefonoFijo;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getTelefonoCelular() {
        return telefonoCelular;
    }

    public String getTelefonoFijo() {
        return telefonoFijo;
    }

    public String getNit() {
        return nit;
    }

    public String getEmail() {
        return email;
    }
}
